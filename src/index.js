import React from "react";
import ReactDOM from "react-dom";
import App from "./components/app/App";
import * as serviceWorker from "./serviceWorker";

import { ActionCableProvider } from "react-actioncable-provider";
import { ApolloProvider } from "react-apollo";
import { ApolloClient } from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { createUploadLink } from "apollo-upload-client";
import { API_WS_ROOT } from "./components/constants";

const link = createUploadLink({
  uri: "http://0.0.0.0:3000/graphql"
});

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: link
});

ReactDOM.render(
  <ActionCableProvider url={API_WS_ROOT}>
    <ApolloProvider client={client}>
      <App />
    </ApolloProvider>
    ,
  </ActionCableProvider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
