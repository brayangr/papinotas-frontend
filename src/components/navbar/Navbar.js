import React from "react";
import routes from "../app/routes";
import { NavLink } from "react-router-dom";

class Navbar extends React.Component {
  render() {
    return (
      <>
        <nav className="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0">
          <a className="navbar-brand col-sm-3 col-md-2 mr-0" href={routes.home}>
            Papinotas-Alumnos
          </a>
        </nav>

        <div className="container-fluid">
          <div className="row">
            <nav
              className="col-md-2 d-none d-md-block bg-light sidebar"
              style={{ position: "fixed", height: "100%" }}
            >
              <div className="sidebar-sticky">
                <ul className="nav flex-column">
                  <li className="nav-item">
                    <NavLink className="nav-link" to={routes.courses}>
                      Cursos
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link" to={routes.periods}>
                      Periodos
                    </NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link" to={routes.students}>
                      Estudiantes
                    </NavLink>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
        </div>
      </>
    );
  }
}

export default Navbar;
