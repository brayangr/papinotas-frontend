export default {
  home: "/",
  courses: "/courses",
  periods: "/periods",
  students: "/students"
};
