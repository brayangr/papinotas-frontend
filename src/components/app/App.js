import React from "react";
import { BrowserRouter } from "react-router-dom";
import { Route } from "react-router-dom";

import CourseIndex from "../course/index";
import CourseCreate from "../course/create";
import Messages from "../messages/index";
import PeriodIndex from "../period/index";
import PeriodCreate from "../period/create";
import StudentIndex from "../student/index";
import StudentCreate from "../student/create";
import Navbar from "../navbar/Navbar";
import routes from "./routes";

function App() {
  return (
    <>
      <BrowserRouter>
        <Navbar />
        <Messages />
        <main role="main" className="col-md-9 ml-sm-auto col-lg-10 pt-3 px-4">
          <Route exact path={routes.courses} component={CourseIndex} />
          <Route
            exact
            path={routes.courses + "/new"}
            component={CourseCreate}
          />
          <Route exact path={routes.periods} component={PeriodIndex} />
          <Route
            exact
            path={routes.periods + "/new"}
            component={PeriodCreate}
          />
          <Route exact path={routes.students} component={StudentIndex} />
          <Route
            exact
            path={routes.students + "/new"}
            component={StudentCreate}
          />
        </main>
      </BrowserRouter>
    </>
  );
}

export default App;
