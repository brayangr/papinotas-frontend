import React from "react";

import "./button.scss";

const Button = props => {
  return (
    <button
      className={"btn btn-" + props.variant + " ml-auto"}
      type={props.type}
      onClick={props.onClick}
      block={props.block}
    >
      {props.text}
    </button>
  );
};

export default Button;
