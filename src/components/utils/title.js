import React from "react";

const Title = props => {
  return (
    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
      <h3>{props.title}</h3>
    </div>
  );
};

export default Title;
