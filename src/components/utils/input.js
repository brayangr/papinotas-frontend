import React from "react";

class Input extends React.Component {
  constructor() {
    super();

    this.input = React.createRef();
  }

  value = () => {
    return this.input.current.value;
  };

  render() {
    return (
      <div className="form-group">
        <label>{this.props.label}</label>
        <input
          required={this.props.required}
          type={this.props.type}
          className="form-control"
          placeholder={this.props.placeholder}
          onChange={this.props.onChange}
          ref={this.input}
        />
      </div>
    );
  }
}

export default Input;
