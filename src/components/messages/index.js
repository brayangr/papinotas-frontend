import React from "react";
import { ActionCableConsumer } from "react-actioncable-provider";
import { ToastContainer } from "react-toastr";

const Messages = props => {
  let container;

  const handleReceivedMessage = response => {
    if (response.success !== undefined) {
      container.success(response.success.toString());
    } else {
      container.error(response.id + " " + response.error.toString());
    }
  };

  return (
    <div className="conversationsList">
      <ActionCableConsumer
        channel={{ channel: "MessagesChannel" }}
        onReceived={handleReceivedMessage}
      />
      <ToastContainer
        ref={ref => (container = ref)}
        className="toast-top-right"
      />
    </div>
  );
};

export default Messages;
