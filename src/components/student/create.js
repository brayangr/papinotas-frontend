import React from "react";
import { Mutation } from "react-apollo";
import gql from "graphql-tag";
import { ToastContainer } from "react-toastr";
import Button from "../utils/button";
import Input from "../utils/input";
import Title from "../utils/title";
import routes from "../app/routes";

const CREATE_STUDENT = gql`
  mutation CreateStudent(
    $name: String!
    $paternalSurname: String!
    $maternalSurname: String!
    $rut: String!
  ) {
    createStudent(
      input: {
        name: $name
        paternalSurname: $paternalSurname
        maternalSurname: $maternalSurname
        rut: $rut
      }
    ) {
      student {
        id
        name
        paternalSurname
        maternalSurname
        rut
      }
      errors
    }
  }
`;

const StudentCreate = props => {
  let name = "";
  let paternalSurname = "";
  let maternalSurname = "";
  let rut = "";
  let container;

  const onSubmit = (e, createStudent) => {
    e.preventDefault();
    createStudent({
      variables: {
        name: name,
        paternalSurname: paternalSurname,
        maternalSurname: maternalSurname,
        rut: rut
      }
    }).then(result => {
      if (result.data.createStudent.errors.length === 0) {
        props.history.replace(routes.students);
      } else {
        container.error(result.data.createStudent.errors.toString());
      }
    });
  };

  return (
    <Mutation mutation={CREATE_STUDENT}>
      {createStudentMutation => (
        <>
          <Title title="Nuevo estudiante" />
          <form onSubmit={e => onSubmit(e, createStudentMutation)}>
            <div className="pb-2 mb-3 border-bottom">
              <div className="row">
                <div className="col-md-12">
                  <Input
                    control_id="name"
                    required={true}
                    label="Nombre"
                    type="text"
                    placeholder="Ingrese Nombre"
                    onChange={e => (name = e.target.value)}
                  />
                </div>
                <div className="col-md-12">
                  <Input
                    control_id="paternalSurname"
                    required={true}
                    label="Apellido Paterno"
                    type="text"
                    placeholder="Ingrese apellido paterno"
                    onChange={e => (paternalSurname = e.target.value)}
                  />
                </div>
                <div className="col-md-12">
                  <Input
                    control_id="maternalSurname"
                    required={true}
                    label="Apellido Materno"
                    type="text"
                    placeholder="Ingrese apellido materno"
                    onChange={e => (maternalSurname = e.target.value)}
                  />
                </div>
                <div className="col-md-12">
                  <Input
                    control_id="rut"
                    required={true}
                    label="Rut"
                    type="text"
                    placeholder="Ingrese rut (sin puntos ni guión)"
                    onChange={e => (rut = e.target.value)}
                  />
                </div>
              </div>
            </div>

            <div className="row">
              <div>
                <Button
                  text="Cancelar"
                  type=""
                  variant="secondary"
                  onClick={() => {
                    props.history.replace(routes.students);
                  }}
                />
                <Button
                  text="Guardar periodo"
                  type="submit"
                  variant="success"
                />
              </div>
            </div>
          </form>
          <ToastContainer
            ref={ref => (container = ref)}
            className="toast-top-right"
          />
        </>
      )}
    </Mutation>
  );
};

export default StudentCreate;
