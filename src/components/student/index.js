import React from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";
import { ToastContainer } from "react-toastr";
import Button from "../utils/button";
import routes from "../app/routes";
import { Mutation } from "react-apollo";

const STUDENTS_QUERY = gql`
  query {
    students {
      id
      name
      paternalSurname
      maternalSurname
      rut
      courses {
        edges {
          node {
            id
            course {
              shortName
            }
            period {
              name
            }
            listNumber
          }
        }
      }
    }
  }
`;

const UPLOAD_FILE = gql`
  mutation createStudentsXlsx($file: Upload!) {
    createStudentsXlsx(input: { file: $file }) {
      errors
    }
  }
`;

const DELETE_STUDENT = gql`
  mutation DeleteStudent($id: ID!) {
    deleteStudent(input: { id: $id }) {
      student {
        id
        name
      }
      errors
    }
  }
`;

const StudentIndex = props => {
  let container;

  const deleteStudent = (event, deleteStudentMutation, id) => {
    deleteStudentMutation({ variables: { id: id } }).then(result => {
      if (result.data.deleteStudent.errors.length === 0) {
        document.getElementById("row-" + id).remove();
        container.success("Estudiante eliminado");
      } else {
        container.error(result.data.deleteStudent.errors.toString());
      }
    });
  };

  const downloadStudents = event => {
    fetch("http://localhost:3000/api/v1/students")
      .then(response => response.blob())
      .then(blob => {
        var url = window.URL.createObjectURL(blob);
        var download = document.createElement("a");
        download.href = url;
        download.download = "nomina_estudiantes.xlsx";
        document.body.appendChild(download);
        download.click();
        download.remove();
      });
  };

  return (
    <>
      <Mutation mutation={UPLOAD_FILE}>
        {uploadFile => (
          <form>
            <div className="custom-file">
              <input
                className="custom-file-input"
                type="file"
                required
                onChange={({
                  target: {
                    validity,
                    files: [file]
                  }
                }) => validity.valid && uploadFile({ variables: { file } })}
              />
              <label className="custom-file-label">
                Cargar estudiantes vía excel
              </label>
            </div>
          </form>
        )}
      </Mutation>
      <Query query={STUDENTS_QUERY}>
        {({ loading, error, data }) => {
          if (loading) return <div>Fetching..</div>;
          if (error) return <div>Error!</div>;
          return (
            <>
              <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                <div className="col-sm-8">
                  <h2>Estudiantes</h2>
                </div>
                <div className="col-sm-4">
                  <div className="row">
                    <Button
                      text="Descargar nomina de estudiantes"
                      type=""
                      variant="outline-dark"
                      onClick={downloadStudents}
                    />
                    <Button
                      text="Nuevo estudiante"
                      type=""
                      variant="outline-dark"
                      onClick={() => {
                        props.history.replace(routes.students + "/new");
                      }}
                    />
                  </div>
                </div>
              </div>

              <table className="table table-hover" size="sm">
                <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Apellido Paterno</th>
                    <th>Apellido Materno</th>
                    <th>Rut</th>
                    <th>Curso</th>
                    <th>N° de Lista</th>
                    <th>Periodo</th>
                    <th>Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  {data.students.map(student => {
                    let course = "";
                    let listNumber = "";
                    let period = "";

                    if (student.courses.edges[0] !== undefined) {
                      course = student.courses.edges[0].node.course.shortName;
                      listNumber = student.courses.edges[0].node.listNumber;
                      period = student.courses.edges[0].node.period.name;
                    }

                    return (
                      <tr key={student.id} id={"row-" + student.id}>
                        <td>{student.name}</td>
                        <td>{student.paternalSurname}</td>
                        <td>{student.maternalSurname}</td>
                        <td>{student.rut}</td>
                        <td>{course}</td>
                        <td>{listNumber}</td>
                        <td>{period}</td>
                        <td>
                          <Mutation mutation={DELETE_STUDENT}>
                            {deleteStudentMutation => (
                              <Button
                                text="Eliminar"
                                type=""
                                variant="danger"
                                onClick={e =>
                                  deleteStudent(
                                    e,
                                    deleteStudentMutation,
                                    student.id
                                  )
                                }
                              />
                            )}
                          </Mutation>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </>
          );
        }}
      </Query>
      <ToastContainer
        ref={ref => (container = ref)}
        className="toast-top-right"
      />
    </>
  );
};

export default StudentIndex;
