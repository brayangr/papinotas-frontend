import React from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";
import { ToastContainer } from "react-toastr";
import Button from "../utils/button";
import routes from "../app/routes";
import { Mutation } from "react-apollo";

const PERIODS_QUERY = gql`
  query {
    periods {
      id
      name
      startDate
      endDate
    }
  }
`;

const UPLOAD_FILE = gql`
  mutation createPeriodsXlsx($file: Upload!) {
    createPeriodsXlsx(input: { file: $file }) {
      errors
    }
  }
`;

const DELETE_PERIOD = gql`
  mutation DeletePeriod($id: ID!) {
    deletePeriod(input: { id: $id }) {
      period {
        id
        name
        startDate
        endDate
      }
      errors
    }
  }
`;

const PeriodIndex = props => {
  let container;

  const deletePeriod = (event, deletePeriodMutation, id) => {
    deletePeriodMutation({ variables: { id: id } }).then(result => {
      if (result.data.deletePeriod.errors.length === 0) {
        document.getElementById("row-" + id).remove();
        container.success("Periodo eliminado");
      } else {
        container.error(result.data.deletePeriod.errors.toString());
      }
    });
  };

  return (
    <>
      <Mutation mutation={UPLOAD_FILE}>
        {uploadFile => (
          <form>
            <div className="custom-file">
              <input
                className="custom-file-input"
                type="file"
                required
                onChange={({
                  target: {
                    validity,
                    files: [file]
                  }
                }) => validity.valid && uploadFile({ variables: { file } })}
              />
              <label className="custom-file-label">
                Cargar periodos vía excel
              </label>
            </div>
          </form>
        )}
      </Mutation>
      <Query query={PERIODS_QUERY}>
        {({ loading, error, data }) => {
          if (loading) return <div>Fetching..</div>;
          if (error) return <div>Error!</div>;
          return (
            <>
              <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                <div className="col-sm-8">
                  <h2>Periodos</h2>
                </div>
                <div className="col-sm-4">
                  <div className="row">
                    <Button
                      text="Nuevo periodo"
                      type=""
                      variant="outline-dark"
                      onClick={() => {
                        this.props.history.replace(routes.periods + "/new");
                      }}
                    />
                  </div>
                </div>
              </div>

              <table className="table table-hover" size="sm">
                <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Fecha de inicio</th>
                    <th>Fecha de término</th>
                    <th>Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  {data.periods.map(period => {
                    return (
                      <tr key={period.id} id={"row-" + period.id}>
                        <td>{period.name}</td>
                        <td>{period.startDate}</td>
                        <td>{period.endDate}</td>
                        <td>
                          <Mutation mutation={DELETE_PERIOD}>
                            {deletePeriodMutation => (
                              <Button
                                text="Eliminar"
                                type=""
                                variant="danger"
                                onClick={e =>
                                  deletePeriod(
                                    e,
                                    deletePeriodMutation,
                                    period.id
                                  )
                                }
                              />
                            )}
                          </Mutation>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </>
          );
        }}
      </Query>
      <ToastContainer
        ref={ref => (container = ref)}
        className="toast-top-right"
      />
    </>
  );
};

export default PeriodIndex;
