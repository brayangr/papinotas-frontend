import React from "react";
import { Mutation } from "react-apollo";
import gql from "graphql-tag";
import { ToastContainer } from "react-toastr";
import Button from "../utils/button";
import Input from "../utils/input";
import Title from "../utils/title";
import routes from "../app/routes";

const CREATE_PERIOD = gql`
  mutation CreatePeriod(
    $name: String!
    $startDate: String!
    $endDate: String!
  ) {
    createPeriod(
      input: { name: $name, startDate: $startDate, endDate: $endDate }
    ) {
      period {
        id
        name
        startDate
        endDate
      }
      errors
    }
  }
`;

const PeriodCreate = props => {
  let name = "";
  let startDate = "";
  let endDate = "";
  let container;

  const onSubmit = (e, createPeriod) => {
    e.preventDefault();
    createPeriod({
      variables: { name: name, startDate: startDate, endDate: endDate }
    }).then(result => {
      if (result.data.createPeriod.errors.length === 0) {
        props.history.replace(routes.periods);
      } else {
        container.error(result.data.createPeriod.errors.toString());
      }
    });
  };

  return (
    <Mutation mutation={CREATE_PERIOD}>
      {createPeriodMutation => (
        <>
          <Title title="Nuevo periodo" />
          <form onSubmit={e => onSubmit(e, createPeriodMutation)}>
            <div className="pb-2 mb-3 border-bottom">
              <div className="row">
                <div className="col-md-6">
                  <Input
                    control_id="name"
                    required={true}
                    label="Nombre del periodo"
                    type="text"
                    placeholder="Ingrese Nombre del curso"
                    onChange={e => (name = e.target.value)}
                  />
                </div>
                <div className="col-md-3">
                  <Input
                    control_id="startDate"
                    required={true}
                    label="Fecha de inicio"
                    type="date"
                    placeholder=""
                    onChange={e => (startDate = e.target.value)}
                  />
                </div>
                <div className="col-md-3">
                  <Input
                    control_id="endDate"
                    required={true}
                    label="Fecha de término"
                    type="date"
                    placeholder=""
                    onChange={e => (endDate = e.target.value)}
                  />
                </div>
              </div>
            </div>

            <div className="row">
              <div>
                <Button
                  text="Cancelar"
                  type=""
                  variant="secondary"
                  onClick={() => {
                    props.history.replace(routes.periods);
                  }}
                />
                <Button
                  text="Guardar periodo"
                  type="submit"
                  variant="success"
                />
              </div>
            </div>
          </form>
          <ToastContainer
            ref={ref => (container = ref)}
            className="toast-top-right"
          />
        </>
      )}
    </Mutation>
  );
};

export default PeriodCreate;
