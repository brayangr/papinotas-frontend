export const API_ROOT = "http://0.0.0.0:3000";
export const API_WS_ROOT = "ws://0.0.0.0:3000/cable";
export const HEADERS = {
  "Content-Type": "application/json",
  Accept: "application/json"
};
