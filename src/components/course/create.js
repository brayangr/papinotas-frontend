import React from "react";
import { Mutation } from "react-apollo";
import gql from "graphql-tag";
import { ToastContainer } from "react-toastr";
import Button from "../utils/button";
import Input from "../utils/input";
import Title from "../utils/title";
import routes from "../app/routes";

const CREATE_COURSE = gql`
  mutation CreateCourse($fullName: String!, $shortName: String!) {
    createCourse(input: { fullName: $fullName, shortName: $shortName }) {
      course {
        id
        fullName
        shortName
      }
      errors
    }
  }
`;

const CourseCreate = props => {
  let fullName = "";
  let shortName = "";
  let container;

  const onSubmit = (e, createCourse) => {
    e.preventDefault();
    createCourse({
      variables: { fullName: fullName, shortName: shortName }
    }).then(result => {
      if (result.data.createCourse.errors.length === 0) {
        props.history.replace(routes.courses);
      } else {
        container.error(result.data.createCourse.errors.toString());
      }
    });
  };

  return (
    <Mutation mutation={CREATE_COURSE}>
      {createCourseMutation => (
        <>
          <Title title="Nuevo curso" />
          <form onSubmit={e => onSubmit(e, createCourseMutation)}>
            <div className="pb-2 mb-3 border-bottom">
              <div className="row">
                <div className="col-md-6">
                  <Input
                    control_id="fullName"
                    required={true}
                    label="Nombre del curso"
                    type="text"
                    placeholder="Ingrese Nombre del curso"
                    onChange={e => (fullName = e.target.value)}
                  />
                </div>
                <div className="col-md-6">
                  <Input
                    control_id="shortName"
                    required={true}
                    label="Nombre Corto (ej: 1°A)"
                    type="text"
                    placeholder="Ingrese nombre corto"
                    onChange={e => (shortName = e.target.value)}
                  />
                </div>
              </div>
            </div>

            <div className="row">
              <div>
                <Button
                  text="Cancelar"
                  type=""
                  variant="secondary"
                  onClick={() => {
                    props.history.replace(routes.courses);
                  }}
                />
                <Button text="Guardar curso" type="submit" variant="success" />
              </div>
            </div>
          </form>
          <ToastContainer
            ref={ref => (container = ref)}
            className="toast-top-right"
          />
        </>
      )}
    </Mutation>
  );
};

export default CourseCreate;
