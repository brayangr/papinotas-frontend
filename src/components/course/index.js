import React from "react";
import { Query } from "react-apollo";
import gql from "graphql-tag";
import { ToastContainer } from "react-toastr";
import Button from "../utils/button";
import routes from "../app/routes";
import { Mutation } from "react-apollo";

const COURSES_QUERY = gql`
  query {
    courses {
      id
      fullName
      shortName
    }
  }
`;

const DELETE_COURSE = gql`
  mutation DeleteCourse($id: ID!) {
    deleteCourse(input: { id: $id }) {
      course {
        id
        fullName
        shortName
      }
      errors
    }
  }
`;

const UPLOAD_FILE = gql`
  mutation createCoursesXlsx($file: Upload!) {
    createCoursesXlsx(input: { file: $file }) {
      errors
    }
  }
`;

const CourseIndex = props => {
  let container;

  const deleteCourse = (event, deleteCourseMutation, id) => {
    deleteCourseMutation({ variables: { id: id } }).then(result => {
      if (result.data.deleteCourse.errors.length === 0) {
        document.getElementById("row-" + id).remove();
        container.success("Curso eliminado");
      } else {
        container.error(result.data.deleteCourse.errors.toString());
      }
    });
  };

  return (
    <>
      <Mutation mutation={UPLOAD_FILE}>
        {uploadFile => (
          <form>
            <div className="custom-file">
              <input
                className="custom-file-input"
                type="file"
                required
                onChange={({
                  target: {
                    validity,
                    files: [file]
                  }
                }) => validity.valid && uploadFile({ variables: { file } })}
              />
              <label className="custom-file-label">
                Cargar cursos vía excel
              </label>
            </div>
          </form>
        )}
      </Mutation>
      <Query query={COURSES_QUERY}>
        {({ loading, error, data }) => {
          if (loading) return <div>Fetching..</div>;
          if (error) return <div>Error!</div>;
          return (
            <>
              <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                <div className="col-sm-8">
                  <h2>Cursos</h2>
                </div>
                <div className="col-sm-4">
                  <div className="row">
                    <Button
                      text="Nuevo curso"
                      type=""
                      variant="outline-dark"
                      onClick={() => {
                        props.history.replace(routes.courses + "/new");
                      }}
                    />
                  </div>
                </div>
              </div>

              <table className="table table-hover" size="sm">
                <thead>
                  <tr>
                    <th>Nombre</th>
                    <th>Código</th>
                    <th>Acciones</th>
                  </tr>
                </thead>
                <tbody>
                  {data.courses.map(course => {
                    return (
                      <tr key={course.id} id={"row-" + course.id}>
                        <td>{course.fullName}</td>
                        <td>{course.shortName}</td>
                        <td>
                          <Mutation mutation={DELETE_COURSE}>
                            {deleteCourseMutation => (
                              <Button
                                text="Eliminar"
                                type=""
                                variant="danger"
                                onClick={e =>
                                  deleteCourse(
                                    e,
                                    deleteCourseMutation,
                                    course.id
                                  )
                                }
                              />
                            )}
                          </Mutation>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </>
          );
        }}
      </Query>
      <ToastContainer
        ref={ref => (container = ref)}
        className="toast-top-right"
      />
    </>
  );
};

export default CourseIndex;
