# Papinotas FrontEnd

This project is the front-end part of the technical test for the papinotas
developer postulation

## How to run

To run this project you need to have installed `docker-compose` and run the following commands

- Since the project is composed by two applications (front-end and back-end), in order to communicate them we need to have both on the same network. To do that run the next command in your terminal emulator.

```
docker network create papinotas
```

- Build the docker image.

```
docker-compose build
```

- Install dependencies.

```
docker-compose run app npm install
```

- Run the container

```
docker-compose up
```

you also need to build and run the back-end project in order to test this app, to do that follow the instructions that are in the [back-end project](https://bitbucket.org/brayangr/papinotas-backend/src/master/)

## Architecture

The architecture for this project is composed by a machine running **_react_** for the front-end, and for the back-end we have a machine running the **_rails application_**, a machine running **_sidekiq_** to run tasks in the background (xlsx files imports), a _redis_ machine to queue the tasks and to use **_action cable_** for the notifications and a **_postgresql_** machine for the database. This description can be seen in the diagram below.

![architecture](./utils/architecture.png)

## Database

The database design was made taking into account the following

- A **_student_** can have more than one **_course_** but only one by period

- A **_period_** can have more than one **_course_**
- A **_course_** can have more than one **_period_**
- The list number is unique by the combination of a **_course_** with a **_period_**
- A **_student_** can have more than one **_course_** including the same **_course_** but in different **_periods_**
- A **_student_** can have only one **_course_** by period

![erd](./utils/erd.png)

## Implementation details

For the implementation i used the next technologies/libraries

- _graphql_ for the communication layer
- _graphql_ for the file upload
- _sidekiq_ to process the xlsx files as a background task
- _redis_ to queue the background tasks
- _action-cable_ to make notifications for the background tasks (succes messages and errors messages)
- To the file download a used a normal API
- The project doesn't have autentication because it was not requested

## Example xlsx files

Here are the files with the format needed to load the data using xlsx

- [courses](./utils/cursos.xlsx)
- [periods](./utils/periodos.xlsx)
- [students](./utils/estudiantes.xlsx)
